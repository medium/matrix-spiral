const { Just, Nothing } = require('./sanctuary');

// Logs matrix to console
//
// renderMatrix :: Matrix => undefined

const renderMatrix = matrix => {
  for (const row of matrix.data) {
    console.log(row.join(' '));
  }
};

// Validate matrix data. Returns false if:
//
// - rows is zero length
// - any row in rows is zero length
// - if each rows is not equals length
//
// validateData :: Array Array a -> Boolean

const validateData = rows => {
  if (typeof rows === 'undefined') return false;
  if (rows.constructor.name !== 'Array' || rows.length === 0)
    return false;

  let rowLength;

  for (const row of rows) {
    if (row.constructor.name !== 'Array' || row.length === 0)
      return false;

    if (typeof rowLength === 'undefined') rowLength = row.length;
    else if (row.length !== rowLength) return false;
  }

  return true;
};

// Create matrix from 2-dimention array
//
// createMatrix :: Array Array a => a -> Maybe(Matrix({ cols: Number, rows: Number, data: a }))
//
// Example:
//
//   createMatrix([[1, 3], [2, 4]]);
//   => Just(Matrix({ cols: 2, rows: 2, data: [[1, 3], [2, 4]] }))
//   createMatrix([[1, 3], 'Oh sh**']);
//   => Nothing

const createMatrix = data => {
  if (!validateData(data)) return Nothing;

  const cols = data[0].length;
  const rows = data.length;

  return Just({ cols, rows, data });
};

// Get value from matrix by coordinates
//
// getValueByCoords :: Matrix -> [Number, Number] -> Maybe
//
// Example:
//
//   cosnt matrix = createMatrix([[1, 2], [3, 4]]).value;
//   getValueByCoords(matrix, [1, 1]);
//   => Just(4)
//   getValueByCoords(matrix, [1, 2]);
//   => Nothing

const getValueByCoords = (matrix, coordinate) => {
  if (typeof coordinate === 'undefined' ||
    coordinate.constructor.name !== 'Array') {

    return Nothing;
  }

  const [x, y] = coordinate;
  if (x < 0 || x >= matrix.cols) return Nothing;
  if (y < 0 || y >= matrix.rows) return Nothing;

  return Just(matrix.data[y][x]);
};

// Generate matrix with exact cols and rows numbers, values generated with generator function
// Generator function apply index of col and row as Number
//
// generateMatrix :: Number a -> Number b -> (Number -> Number -> c) -> Maybe(Matrix({ cols: a, rows: b, data: Array Array c }))
//
// Example:
//
//   generateMatrix(2, 2, (col, row) => `${row}${col}`);
//   => Just(Matrix({ cols: 2, rows: 2, data: [['00', '01'], ['10', '11']] }))
//   generateMatrix(1, () => 'Ok']);
//   => Just(Matrix({ cols: 1, rows: 1, data: [['Ok']] }))
//   generateMatrix(0, () => 'Oh sh**']);
//   => Nothing

const generateMatrix = (cols, rows, generatorFn) => {
  const data = [];

  if (typeof rows === 'function') {
    generatorFn = rows;
    rows = cols;
  }

  for (let i = 0; i < rows; i++) {
    const row = [];
    for (let j = 0; j < cols; j++) {
      row.push(generatorFn(j, i));
    }
    data.push(row);
  }

  return createMatrix(data);
};


module.exports = {
  createMatrix,
  generateMatrix,
  getValueByCoords,
  renderMatrix
};
