const { getValueByCoords } = require('./matrix');

// Generate sequence of direction vectors

const spiralVectorsGen = function* () {
  for (;;) {
    yield [-1, 0]; // left
    yield [ 0, 1]; // bottom
    yield [ 1, 0]; // right
    yield [ 0, -1]; // top
  }
};

// Iterate over matrix value by spiral

const matrixSpiralGen = function* (matrix) {
  const middle = Math.floor(matrix.cols / 2);
  const vectorIter = spiralVectorsGen();

  let cursor = [middle, middle];
  let vector = vectorIter.next().value;
  let counts = 1;
  let iteration = 1;

  for (;;) {
    const number = getValueByCoords(matrix, cursor);

    if (number.isNothing) break;
    else yield number.value;

    if (counts === 0) {
      iteration++;
      vector = vectorIter.next().value;
      counts = Math.ceil(iteration / 2);
    }

    cursor = [cursor[0] + vector[0], cursor[1] + vector[1]];
    counts--;
  }
};

module.exports = { spiralVectorsGen, matrixSpiralGen };
