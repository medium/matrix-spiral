const { expect } = require('chai');

const { createMatrix } = require('../lib/matrix');
const {
  spiralVectorsGen, matrixSpiralGen
} = require('../lib/spiral-gen');

describe('Spiral Generator module', function() {
  describe('.spiralVectorsGen', function() {
    const vectorIter = spiralVectorsGen();

    it('should generate proper vector sequence', function() {
      const vectors = [];
      for (let i = 0; i < 5; i++) {
        vectors.push(vectorIter.next().value);
      }

      expect(vectors).to.deep.equal([
        [-1, 0], [0, 1], [1, 0], [0, -1], [-1, 0]
      ])
    });
  });

  describe('.matrixSpiralGen', function() {
    const matrix = createMatrix([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9]
    ]).value;
    const valueSquence = matrixSpiralGen(matrix);

    it('should generate proper spiral values sequence', function() {
      const values = Array.from(valueSquence);
      expect(values).to.deep.equal([5, 4, 7, 8, 9, 6, 3, 2, 1]);
    });
  });
});
