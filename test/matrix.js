const { expect } = require('chai');

const {
  createMatrix, generateMatrix, getValueByCoords
} = require('../lib/matrix');

describe('Matrix module', function() {
  describe('.createMatrix', function() {
    context('with proper income data', function() {
      const matrix = createMatrix([[1, 2], [3, 4], [5, 6]]);

      it('should return Just', function() {
        expect(matrix.isJust).to.be.true;
      });

      it('should contains proper object', function() {
        expect(matrix.value).to.have.all.keys('cols', 'rows', 'data');
      });

      it('should contains proper data', function() {
        const target = {
          cols: 2, rows: 3,
          data: [[1, 2], [3, 4], [5, 6]]
        };
        expect(matrix.value).to.deep.equal(target);
      }); 
    });

    context('with bad income data', function() {
      it('should return nothing with empty array', function() {
        const matrix = createMatrix([]);
        expect(matrix.isNothing).to.be.true;
      });

      it('should return nothing with unnormalized data', function() {
        const matrix = createMatrix([[1, 2], [3]]);
        expect(matrix.isNothing).to.be.true;
      });

      it('should return nothing with no arguments passed', function() {
        const matrix = createMatrix();
        expect(matrix.isNothing).to.be.true;
      });

      it('should return nothing with not array data', function() {
        const matrix = createMatrix('some wrong data');
        expect(matrix.isNothing).to.be.true;
      });
    });
  });

  describe('.generateMatrix', function() {
    context('with proper income data', function() {
      const generatorFn = (col, row) => `${row}${col}`;

      it('should return Just', function() {
        const matrix = generateMatrix(2, 3, generatorFn);
        expect(matrix.isJust).to.be.true;
      });

      it('should contains proper object', function() {
        const matrix = generateMatrix(2, 3, generatorFn);
        expect(matrix.value).to.have.all.keys('cols', 'rows', 'data');
      });

      it('should contains proper data', function() {
        const matrix = generateMatrix(2, 3, generatorFn);
        const target = {
          cols: 2, rows: 3,
          data: [
            ['00', '01'],
            ['10', '11'],
            ['20', '21']
          ]
        };
        expect(matrix.value).to.deep.equal(target);
      }); 
    });

    context('with bad income data', function() {
      it('should return nothing with no arguments', function() {
        const matrix = generateMatrix();
        expect(matrix.isNothing).to.be.true;
      });

      it('should return nothing with unproper sizes', function() {
        const matrix = generateMatrix(0, () => 'Wrong');
        expect(matrix.isNothing).to.be.true;
      });

      it('should return nothing with no generation function', function() {
        const matrix = generateMatrix(3);
        expect(matrix.isNothing).to.be.true;
      });
    });
  });

  describe('.getValueByCoords', function() {
    const matrix = generateMatrix(5, 5, (col, row) => `${col}${row}`)
      .value;

    it('should return Just if coords are in the target', function() {
      const result = getValueByCoords(matrix, [2, 3]);
      expect(result.isJust).to.be.true;
    });

    it('should return proper value by coords', function() {
      const value = getValueByCoords(matrix, [2, 3]).value;
      expect(value).to.equal('23');
    });

    it('should return Nothing if coords is outside of data', function() {
      const result = getValueByCoords(matrix, [100, 2]);
      expect(result.isNothing).to.be.true;
    });

    it('should return Nothing with bad coords', function() {
      const result = getValueByCoords(matrix, 'bad coords');
      expect(result.isNothing).to.be.true;
    });
  });
});
